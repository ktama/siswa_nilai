<!DOCTYPE html>
<html>
<head>
    <title>Laravel CRUD Application</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css" rel="stylesheet">
</head>

<body>
	
    @if(Auth::check())
<div class="container">
    @yield('content')
</div>
	@endif
	
	@if(Auth::guest())
   <a href="/login" class="btn btn-info"> Silahkan login terlebih dahulu >></a>
    @endif
</body>
</html>
