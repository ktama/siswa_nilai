@extends('siswa_nilai.layout')


@section('content')

    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-left">

                <h2> Show Data</h2>

            </div>

            <div class="pull-right">

                <a class="btn btn-primary" href="{{ route('siswa_nilai.index') }}"> Back</a>

            </div>

        </div>

    </div>
   

    <div class="row">
		
	
	
	  <div class="col-xs-12 col-sm-12 col-md-12">
         <div class="form-group">
            <strong>Kode Propinsi:</strong>
            {{ $dataSiswa->KD_PROP }}
           
         </div>            
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
         <div class="form-group">
            <strong>Kode Rayon:</strong>
            {{ $dataSiswa->KD_RAYO }}
         </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
         <div class="form-group">
            <strong>Kode SR:</strong>            
            {{ $dataSiswa->KD_SR }}            
         </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
         <div class="form-group">
            <strong>Kode Sekolah:</strong>            
            {{ $dataSiswa->KD_SEK }}           
         </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
         <div class="form-group">
            <strong>Nomor Peserta:</strong>            
            {{ $dataSiswa->NOPES }}            
         </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
         <div class="form-group">
            <strong>Nama Peserta:</strong>            
            {{ $dataSiswa->NM_PES }}            
         </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
         <div class="form-group">
            <strong>NISN:</strong>            
            {{ $dataSiswa->NISN }}            
         </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
         <div class="form-group">
            <strong>Jenis Kelamin:</strong>            
            {{ $dataSiswa->SEX }}            
         </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
         <div class="form-group">
            <strong>Tempat Kelahiran:</strong>            
            {{ $dataSiswa->TMP_LHR }}            
         </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
         <div class="form-group">
            <strong>Tanggal Lahir:</strong>            
            {{ $dataSiswa->TGL_LHR }}            
         </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
         <div class="form-group">
            <strong>Alamat:</strong>            
            {{ $dataSiswa->ALAMAT_1 }}            
         </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
         <div class="form-group">
            <strong>IND:</strong>            
            {{ $dataSiswa->IND }}            
         </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
         <div class="form-group">
            <strong>MAT:</strong>            
            {{ $dataSiswa->MAT }}            
         </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
         <div class="form-group">
            <strong>IPA:</strong>            
            {{ $dataSiswa->IPA }}            
         </div>
      </div>
	
    </div>

@endsection
