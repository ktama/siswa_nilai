@extends('siswa_nilai.layout')

@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Add New Data</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('siswa_nilai.index') }}"> Back</a>
        </div>
    </div>
</div>

   

@if ($errors->any())

    <div class="alert alert-danger">

        <strong>Whoops!</strong> There were some problems with your input.<br><br>

        <ul>

            @foreach ($errors->all() as $error)

                <li>{{ $error }}</li>

            @endforeach

        </ul>

    </div>

@endif

   

<form action="{{ route('siswa_nilai.store') }}" method="POST">

    {{ csrf_field() }}

     <div class="row">
	 <div class="col-xs-12 col-sm-12 col-md-12">
     <div class="form-group">
         <strong>Kode Propinsi:</strong>
         <input type="text" name="KD_PROP" class="form-control" placeholder="Kode Propinsi">
      </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
         <strong>Kode Rayon:</strong>
         <input type="text" name="KD_RAYO" class="form-control" placeholder="Kode Rayon">
      </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
         <strong>Kode SR:</strong>
         <input type="text" name="KD_SR" class="form-control" placeholder="Kode SR">
      </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
         <strong>Kode Sekolah:</strong>
         <input type="text" name="KD_SEK" class="form-control" placeholder="Kode Sekolah">
      </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
         <strong>Nomor Peserta:</strong>
         <input type="text" name="NOPES" class="form-control" placeholder="Nomor Peserta">
      </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
         <strong>Nama Peserta:</strong>
         <input type="text" name="NM_PES" class="form-control" placeholder="Nama Peserta">
      </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
         <strong>NISN:</strong>
         <input type="text" name="NISN" class="form-control" placeholder="Nomo Induk Siswa Nasional">
      </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
         <strong>Jenis Kelamin:</strong>
         <input type="text" name="SEX" class="form-control" placeholder="Jenis Kelamin">
      </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
         <strong>Tempat Kelahiran:</strong>
         <input type="text" name="TMP_LHR" class="form-control" placeholder="Tempat Lahir">
      </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
         <strong>Tanggal Lahir:</strong>
         <input type="text" name="TGL_LHR" class="form-control" placeholder="Tanggal Lahir">
      </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
         <strong>Alamat:</strong>
         <input type="text" name="ALAMAT_1" class="form-control" placeholder="Alamat">
      </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
         <strong>IND:</strong>
         <input type="text" name="IND" class="form-control" placeholder="Nilai Bahasa Indonesia">
      </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
         <strong>MAT:</strong>
         <input type="text" name="MAT" class="form-control" placeholder="Nilai Matematika">
      </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="form-group">
         <strong>IPA:</strong>
         <input type="text" name="IPA" class="form-control" placeholder="Nilai IPA">
      </div>

        <div class="col-xs-12 col-sm-12 col-md-12 text-center">

                <button type="submit" class="btn btn-primary">Submit</button>

        </div>

    </div>

   

</form>

@endsection
