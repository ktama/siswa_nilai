@extends('siswa_nilai.layout')

@section('content')

    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-left">

                <h2>Edit Data</h2>

            </div>

            <div class="pull-right">

                <a class="btn btn-primary" href="{{ route('siswa_nilai.index') }}"> Back</a>

            </div>

        </div>

    </div>

   

    @if ($errors->any())

        <div class="alert alert-danger">

            <strong>Whoops!</strong> There were some problems with your input.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif

  

    <form action="{{ route('siswa_nilai.update',$dataSiswa->id) }}" method="POST">

       <input type="hidden" name="_token" value="{{ csrf_token() }}">
		{{ csrf_field() }}
        {{ method_field('PUT') }}

   

         <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12">
         <div class="form-group">
            <strong>Kode Propinsi:</strong>
            <input type="text" name="KD_PROP" value="{{ $dataSiswa->KD_PROP }}" class="form-control" placeholder="Kode Propinsi">
         </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
         <div class="form-group">
            <strong>Kode Propinsi:</strong>
            <input type="text" name="KD_RAYO" value="{{ $dataSiswa->KD_RAYO }}" class="form-control" placeholder="Kode Rayon">
         </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
         <div class="form-group">
            <strong>Kode SR:</strong>
            <input type="text" name="KD_SR" value="{{ $dataSiswa->KD_SR }}" class="form-control" placeholder="Kode SR">
         </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12">
         <div class="form-group">
            <strong>Kode Sekolah:</strong>
            <input type="text" name="KD_SEK" value="{{ $dataSiswa->KD_SEK }}" class="form-control" placeholder="Kode Sekolah">
         </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
         <div class="form-group">
            <strong>Nomor Peserta:</strong>
            <input type="text" name="NOPES" value="{{ $dataSiswa->NOPES }}" class="form-control" placeholder="Nomor Peserta">
         </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
         <div class="form-group">
            <strong>Nama Peserta:</strong>
            <input type="text" name="NM_PES" value="{{ $dataSiswa->NM_PES }}" class="form-control" placeholder="Nama Peserta">
         </div>
      </div>
  
      <div class="col-xs-12 col-sm-12 col-md-12">
         <div class="form-group">
            <strong>Nomor Induk:</strong>
            <input type="text" name="NISN" value="{{ $dataSiswa->NISN }}" class="form-control" placeholder="Nama Peserta">
         </div>
      </div>
 
      <div class="col-xs-12 col-sm-12 col-md-12">
         <div class="form-group">
            <strong>Jenis Kelamin:</strong>
            <input type="text" name="SEX" value="{{ $dataSiswa->SEX }}" class="form-control" placeholder="Nama Peserta">
         </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
         <div class="form-group">
            <strong>Tempat Kelahiran:</strong>
            <input type="text" name="TMP_LHR" value="{{ $dataSiswa->TMP_LHR }}" class="form-control" placeholder="Tempat Lahir">
         </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
         <div class="form-group">
            <strong>Tanggal Lahir:</strong>
            <input type="text" name="TGL_LHR" value="{{ $dataSiswa->TGL_LHR }}" class="form-control" placeholder="Tanggal Lahir">
         </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
         <div class="form-group">
            <strong>Alamat:</strong>
            <input type="text" name="ALAMAT_1" value="{{ $dataSiswa->ALAMAT_1 }}" class="form-control" placeholder="Alamat">
         </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
         <div class="form-group">
            <strong>IND:</strong>
            <input type="text" name="IND" value="{{ $dataSiswa->IND }}" class="form-control" placeholder="Nilai Bahasa Indonesia">
         </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
         <div class="form-group">
            <strong>MAT:</strong>
            <input type="text" name="MAT" value="{{ $dataSiswa->MAT }}" class="form-control" placeholder="Nilai Matematika">
         </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
         <div class="form-group">
            <strong>IPA:</strong>
            <input type="text" name="IPA" value="{{ $dataSiswa->IPA }}" class="form-control" placeholder="Nilai IPA">
         </div>
      </div>

            <div class="col-xs-12 col-sm-12 col-md-12 text-center">

              <button type="submit" class="btn btn-primary">Submit</button>

            </div>

        </div>

   

    </form>

@endsection
