@extends('siswa_nilai.layout')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
				<a class="btn btn-danger" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
				Logout
				</a>
				<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
				{{ csrf_field() }}
				</form>
                <h2>Laravel CRUD Application</h2>
            </div>
            <div class="pull-right">
             <a class="btn btn-success" href="{{ route('siswa_nilai.create') }}"> Create New Data</a>
            </div>
             
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>KD_PROP</th> 
			<th>KD_RAYO</th>
			<th>KD_SR</th>
			<th>KD_SEK</th>
			<th>NOPES</th>
			<th>NM_PES</th>
			<th>NISN</th>
			<th>SEX</th>
			<th>TMP_LHR</th>
			<th>TGL_LHR</th>
			<th>ALAMAT_1</th>
			<th>IND</th>
			<th>MAT</th>
			<th>IPA</th>
            <th width="280px">Action</th>

        </tr>

        @foreach ($siswa_nilai as $dataSiswa)

        <tr>

            <td>{{ ++$i }}</td>
            <td>{{ $dataSiswa->KD_PROP }}</td>
            <td>{{ $dataSiswa->KD_RAYO }}</td>
            <td>{{ $dataSiswa->KD_SR }}</td>
            <td>{{ $dataSiswa->KD_SEK }}</td>
            <td>{{ $dataSiswa->NOPES }}</td>
            <td>{{ $dataSiswa->NM_PES }}</td>
            <td>{{ $dataSiswa->NISN }}</td>
            <td>{{ $dataSiswa->SEX }}</td>
            <td>{{ $dataSiswa->TMP_LHR }}</td>
            <td>{{ $dataSiswa->TGL_LHR }}</td>
            <td>{{ $dataSiswa->ALAMAT_1 }}</td>
            <td>{{ $dataSiswa->IND }}</td>
            <td>{{ $dataSiswa->MAT }}</td>
            <td>{{ $dataSiswa->IPA }}</td>
                                 
            <td>

                <form action="{{ route('siswa_nilai.destroy',$dataSiswa->id) }}" method="POST">

   

                    <a class="btn btn-info" href="{{ route('siswa_nilai.show',$dataSiswa->id) }}">Show</a>

    

                    <a class="btn btn-primary" href="{{ route('siswa_nilai.edit',$dataSiswa->id) }}">Edit</a>

   

                    {{ csrf_field() }}

                    {{ method_field('DELETE') }}

      

                    <button type="submit" class="btn btn-danger">Delete</button>

                </form>
            </td>
        </tr>

        @endforeach

    </table>

  

    {!! $siswa_nilai->links() !!}

      

@endsection
