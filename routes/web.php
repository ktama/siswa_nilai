<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', function () {
    return redirect('siswa_nilai');
});


Route::get('dataSiswa', function () {

    $dataSiswa = DB::table('siswa_nilai')->get();

    return view('siswa_nilai', ['siswa_nilai' => $dataSiswa]);
});

Route::get('/siswa_nilai/create', 'DataSiswaController@create');
Route::get('/siswa_nilai/{id}', 'DataSiswaController@show');
Route::post('/siswa_nilai/{id}/edit', 'DataSiswaController@edit');
Route::get('delete/{id}','DataSiswaController@destroy');

Route::resource('siswa_nilai','DataSiswaController');


