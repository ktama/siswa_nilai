<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DataSiswa extends Model
{
	public $table = 'siswa_nilai';
	protected $primaryKey = 'id';

    protected $fillable = [
	'KD_PROP', 
	'KD_RAYO',
	'KD_SR',
	'KD_SEK',
	'NOPES',
	'NM_PES',
	'NISN',
	'SEX',
	'TMP_LHR',
	'TGL_LHR',
	'ALAMAT_1',
	'IND',
	'MAT',
	'IPA'
	];
	
	
}
