<?php

namespace App\Http\Controllers;

use App\DataSiswa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Quotation;

class DataSiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $siswa_nilai = DataSiswa::latest()->paginate(10);

        return view('siswa_nilai.index',compact('siswa_nilai'))
			   ->with('i', (request()->input('page', 1) - 1) * 10);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('siswa_nilai.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

            'KD_PROP' => 'required',
            'KD_RAYO' => 'required',
            'KD_SR' => 'required',
			'KD_SEK' => 'required',
			'NOPES' => 'required',
			'NM_PES' => 'required',
			'NISN' => 'required',
			'SEX' => 'required',
            'TMP_LHR' => 'required',
            'TGL_LHR' => 'required',
            'ALAMAT_1' => 'required',
            'IND' => 'required',
            'MAT' => 'required',
            'IPA' => 'required',
        ]);

  

        DataSiswa::create($request->all());

   

        return redirect()->route('siswa_nilai.index')
                         ->with('success','DataSiswa created successfully.');
	}

    /**
     * Display the specified resource.
     *
     * @param  \App\DataSiswa  $dataSiswa
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		$siswa_nilai = DataSiswa::find($id);
		return view('siswa_nilai.show')
			   ->with('dataSiswa',$siswa_nilai);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DataSiswa  $dataSiswa
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$siswa_nilai = DataSiswa::find($id);
		return view('siswa_nilai.edit')
			   ->with('dataSiswa',$siswa_nilai);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DataSiswa  $dataSiswa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
		'KD_PROP' => 'required',
		'KD_RAYO' => 'required',
		'KD_SR' => 'required',
		'KD_SEK' => 'required',
		'NOPES' => 'required',
		'NM_PES' => 'required',
		'NISN' => 'required',
		'SEX' => 'required',
		'TMP_LHR' => 'required',
		'TGL_LHR' => 'required',
		'ALAMAT_1' => 'required',
		'IND' => 'required',
		'MAT' => 'required',
		'IPA' => 'required',
        ]);
        
	$siswa_nilai = DataSiswa::find($id);
	$siswa_nilai->update($request->all());

	return redirect()->route('siswa_nilai.index')
			->with('success','Data telah berhasil diperbaharui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DataSiswa  $dataSiswa
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		$siswa_nilai = DataSiswa::find($id);
        $siswa_nilai->delete($id);
        return redirect()->route('siswa_nilai.index')
                         ->with('success','Data telah berhasil dihapus');

    }
}
